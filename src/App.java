import com.devcamp.books.Author;
import com.devcamp.books.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nguyễn Nhật Ánh", "nhatanh@gmail.com", 'm');
        Author author2 = new Author("Kim Dung", "dung.kim@gmail.com", 'f');

        System.out.println(author1);
        System.out.println(author2);

        Book book1 = new Book("Tôi thấy hoa vàng trên cỏ xanh", author1, 350000);
        Book book2 = new Book("Thần điêu đại hiệp", author2, 1000000, 1000);

        System.out.println(book1);
        System.out.println(book2);
    }
}
